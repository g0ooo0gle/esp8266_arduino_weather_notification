/*
LEDで天気予報をチカチカさせるもの。
AitendoのLEDユニットで動くかもしれない。日本では使えないね。（意味無し）
玄関とかで天気を知る事を目標としている。
プログラムガバガバだから、多少はね？

Webserver　を立ててブラウザでコントロール可能。（予定）
wificonfig　を組み込み済み。単体でAP設定可能。
arduinoOTA　実装済みなのでワイヤレスでプログラム書き込み可能。
ESP8266のフラッシュメモリにSPIFFSファイルシステムを使っているので、別途データ領域にHTMLをアップロードすること。
https://github.com/esp8266/arduino-esp8266fs-plugin

<参考>
http://qiita.com/tadfmac/items/17448a2d96bd56373a66

*/

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <ESP8266mDNS.h>        
#include <WiFiUdp.h>             
#include <ArduinoOTA.h>         
#include <FS.h>
                   
//////////ファイルシステム用設定//////////
const char* path_root   = "/index.html";
#define BUFFER_SIZE 16384         //クソデカバッファ
uint8_t buf[BUFFER_SIZE];
////////////////////

//////////各種ピン定義//////////
const int led = 2;            //アクセスランプ用LED定義 　※arduino互換ボードのLEDは2番
const int LEDoutput = 5;      //ブレッドボードのLED
const int relayout = 4;       //リレー用out
////////////////////

//////////Webサーバーの生成//////////
ESP8266WebServer server(80);
////////////////////


//////////ファイルシステムからHTML読み出し//////////
boolean readHTML(){
  File htmlFile = SPIFFS.open(path_root, "r");        // 読み込みモードでファイルOpen
  if (!htmlFile) {
    Serial.println("Failed to open index.html");
    return false;
  }
  size_t size = htmlFile.size();      // ファイルサイズ取得
  if(size >= BUFFER_SIZE){
    Serial.print("File Size Error:");
    Serial.println((int)size);
  }else{
    Serial.print("File Size OK:");
    Serial.println((int)size);
  }
  htmlFile.read(buf,size);       // バッファにファイルサイズ分読み込み。
  htmlFile.close();
  return true;
}
////////////////////

//////////サーバリクエスト受信時の正常処理時////////
void handleRoot() {
  digitalWrite ( led, 1 );
  Serial.println("Access");
  server.send ( 200, "text/html",(char *)buf);
  digitalWrite ( led, 0 );
}
////////////////////

//////////サーバリクエスト受信時の異常処理時//////////　
//定義されたURI以外にアクセスされた場合の処理を書く
void handleNotFound(){
  
  digitalWrite(led, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++){
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "text/plain", message);
  digitalWrite(led, 0);
}
////////////////////


void setup() {
    
    pinMode(relayout,OUTPUT);
    pinMode(LEDoutput,OUTPUT);
    pinMode(led, OUTPUT);
    digitalWrite(led, 0);
    Serial.begin(115200);
    Serial.println("");

    //////////ファイルシステム//////////
    //ファイルシステム開始
    SPIFFS.begin();
    if(!readHTML()){
      Serial.println("Read HTML error!!");
    }
    ////////////////////

    //////////WiFiManager（自動接続）//////////
  
    // put your setup code here, to run once:
    Serial.begin(115200);

    //WiFiManager
    //Local intialization. Once its business is done, there is no need to keep it around
    WiFiManager wifiManager;
    //reset saved settings
    //wifiManager.resetSettings();
    
    //set custom ip for portal 　
    //Sets a custom ip / gateway / subnet configuration
    
    //Custom Access Point IP Configuration APモードのIP固定(たぶん動作しない)
    //wifiManager.setAPConfig(IPAddress(192,168,1,0), IPAddress(192,168,1,1), IPAddress(255,255,255,0));

    //Custom Station (client) Static IP Configuration クライアント固定IP設定
    wifiManager.setSTAStaticIPConfig(IPAddress(192,168,1,38), IPAddress(192,168,1,1), IPAddress(255,255,255,0));

    //fetches ssid and pass from eeprom and tries to connect
    //if it does not connect it starts an access point with the specified name
    //here  "AutoConnectAP"
    //and goes into a blocking loop awaiting configuration
    wifiManager.autoConnect("AutoConnectAP");
    //or use this for auto generated name ESP + ChipID
    //wifiManager.autoConnect();

    
    //if you get here you have connected to the WiFi
    Serial.println("Yattane! connected...yeey :)");

    //////////WiFiManagerここまで//////////


    //////////ArduinoOTA//////////
    Serial.println("ArduinoOTABooting");
    //アクセスポイント機能を切るらしい
    WiFi.mode(WIFI_STA);
    //コネクション張れてると思うけど、一応
    while (WiFi.waitForConnectResult() != WL_CONNECTED) {
      Serial.println("Connection Failed! Rebooting...");
      delay(5000);
      ESP.restart();
    }

    // Port defaults to 8266
    // ArduinoOTA.setPort(8266);

    // Hostname defaults to esp8266-[ChipID]
    // ArduinoOTA.setHostname("myesp8266");

    // No authentication by default
    // ArduinoOTA.setPassword((const char *)"123");

    ArduinoOTA.onStart([]() {
      Serial.println("Start");
    });
    ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
    ArduinoOTA.begin();
    Serial.println("Ready");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    //////////ArduinoOTAここまで//////////      
    
    //////////webサーバ//////////
    //一応デバックのため表示
    Serial.println(WiFi.localIP());

    //名前解決。mDNS（マルチキャストDNS）いらないかも。
    if (MDNS.begin("esp8266")) {
      Serial.println("MDNS responder started");
    }
    
    /*
    //名前解決。mDNS（マルチキャストDNS）でLAN内で名前でアクセスできるようにする。なんかうまく動かないね。
    if (!MDNS.begin("ESP8266swich")) {
      Serial.println("Error setting up MDNS responder!");
      while(1) { 
        delay(1000);
      }
    }
    */    
    
    //Webサーバの設定を行い、サーバを起動する
    server.on("/", handleRoot);//ルートに接続要求があった時の処理を指定
    server.on("/ON", ON_command);
    server.on("/OFF", OFF_command);
    
    server.on("/inline", [](){
      server.send(200, "text/plain", "this works as well");//Webサーバの接続要求待ち
    });

    server.onNotFound(handleNotFound);

    //サーバーを起動する.
    server.begin();
    Serial.println("HTTP server started");

    /*
    //mDNS起動
    MDNS.addService("http", "tcp", 80);
    */
    
    //////////webサーバここまで//////////
}

void loop() {
  
    //ArduinoOTA
    ArduinoOTA.handle();

    //webサーバ
    server.handleClient();
      
}

//////////フォームボタン処理////////
//ONボタン押下時処理
void ON_command() {
  //デバッグ用表示
  Serial.println("ON_button has been pressed!");
  //スイッチONOFF
  digitalWrite ( LEDoutput, 0 );
  digitalWrite ( relayout, 0 );
  delay(1000);
  digitalWrite ( LEDoutput, 1 );
  digitalWrite ( relayout, 1 );
  delay(1000);
  digitalWrite ( LEDoutput, 0 );
  digitalWrite ( relayout, 0 );
  handleRoot();
}

//OFFボタン押下時処理
void OFF_command() {
  //デバッグ用表示
  Serial.println("ON_button has been pressed!");
  //スイッチONOFF
  digitalWrite ( LEDoutput, 0 );
  digitalWrite ( relayout, 0 );
  delay(1000);
  digitalWrite ( LEDoutput, 1 );
  digitalWrite ( relayout, 1 );
  delay(8000);
  digitalWrite ( LEDoutput, 0 );
  digitalWrite ( relayout, 0 );
  handleRoot();

}
////////////////////
